import pika, sys, os

def process_message(ch, method, properties, body):
    print(" Received %r" % body)

def main():
	# set hostname to connect to
	parameters = pika.ConnectionParameters(host= 'rabbitmq')

	# create connection to rabbitmq
	connection = pika.BlockingConnection(parameters)

	# open channel to rabbitmq
	channel = connection.channel()

	# create queue if doesn't exist
	channel.queue_declare(queue='tasks')

	# configure consumer to call the process_message function
	# when message arrives
	channel.basic_consume(
		queue='tasks',
		on_message_callback=process_message,
		auto_ack=True,
	)

	print(' [*] Waiting for messages. To exit press CTRL+C')

	# tell rabbitmq you're ready to receive messages
	channel.start_consuming()

# just extra stuff to do when the script runs
if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		print('Interrupted')
		try:
			sys.exit(0)
		except SystemExit:
			os.exit(0)