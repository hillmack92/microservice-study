import pika
# set hostname that we'll connect to
parameters = pika.ConnectionParameters(host='rabbitmq')
# create a connection to rabbitmq
connection = pika.BlockingConnection(parameters)
# open a channel to rabbitmq
channel = connection.channel()
# create a queue if it doesn't exist
channel.queue_declare(queue='tasks')
# send message to the queue
channel.basic_publish(exchange='', routing_key='tasks', body='Data from producer')
print("  Sent 'Hello World!'")
connection.close