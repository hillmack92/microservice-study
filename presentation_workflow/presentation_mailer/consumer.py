import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    print('print process approval method')
    message = json.loads(body)
    # status = 'APPROVED'
    send_mail(
        'Congrats, you\'re presentation has been approved!',
        message['presenter_name'] + " your presentation " + message['title'] + ' has been approved!',
        'admin@conference.go',
        [message['presenter_email']],
        fail_silently=False,
    )

def process_rejection(ch, method, properties, body):
    print('print process rejection method')
    message = json.loads(body)
    # status  = 'REJECTED'
    send_mail(
        'Sorry to inform you, but you\'re presentation has been rejected.',
        message['presenter_name'] + ", it's unfortunate that your presentation " + message['title'] + ' has been rejected.',
        'admin@conference.go',
        [message['presenter_email']],
        fail_silently=False,
    )

# RABBITMQ CHANNEL SETUP

parameters = pika.ConnectionParameters(host='rabbitmq')     # takes in name of host container
connection = pika.BlockingConnection(parameters)         #creates channel
channel = connection.channel()

# APPROVALS QUEUE SETUP

channel.queue_declare(queue='presentation_approvals')
channel.basic_consume(
  queue='presentation_approvals',
  on_message_callback=process_approval,
  auto_ack=True,
)

# REJECTION QUEUE SETUP

channel.queue_declare(queue='presentation_rejections')
channel.basic_consume(
  queue='presentation_rejections',
  on_message_callback=process_rejection,
  auto_ack=True,
)

channel.start_consuming()

# send_mail(
#   'Subject here',
#   'Here is the message.',
#   'from@example.com',
#   ['to@example.com'],
#   fail_silently=False,
# )

# def process_message(ch, method, properties, body):
#     print("  Received %r" % body)
#     presentation=json.loads(body)
#     print(presentation)
#     if presentation['presentation_status']=="APPROVED":
#         send_mail(
#             'Your presentation has been accepted',
#             f"{presentation['presenter_name']}, we're happy to tell you that your presentation {presentation_name} has been accepted"
#         )
#     elif presentation['presentation_status']=="REJECTED":
#         send_mail(
#             'Your presentation has been rejected',
#             f"{presentation['presenter_name']}, we're happy to tell you that your presentation {presentation_name} has been rejected"
#         )